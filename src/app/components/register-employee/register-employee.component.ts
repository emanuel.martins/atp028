import { Component, OnInit } from '@angular/core';
import { IFuncionario } from 'src/app/models/funcionario';
import { StorageService } from 'src/app/services/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register-employee',
  templateUrl: './register-employee.component.html',
  styleUrls: ['./register-employee.component.css'],
})
export class RegisterEmployeeComponent implements OnInit {
  private funcionarioStorageService: StorageService;
  public funcionario: IFuncionario;

  constructor() {
    this.funcionarioStorageService = new StorageService();
    this.funcionario = {
      nome: '',
      cargo: '',
      salario: null,
    };
  }

  ngOnInit(): void {}

  save() {
    this.funcionarioStorageService.set(
      '@funcionario',
      JSON.stringify(this.funcionario)
    );
    this.clearForm();
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500,
    });
  }

  clearForm() {
    this.funcionario = {
      nome: '',
      cargo: '',
      salario: null,
    };
  }
}
