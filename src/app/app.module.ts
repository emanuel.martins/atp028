import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { RegisterEmployeeComponent } from './components/register-employee/register-employee.component';
import { ListEmployeeComponent } from './components/list-employee/list-employee.component';
import { FooterComponent } from './components/footer/footer.component';
import { AppComponent } from './components/start/app.component';
import { FormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterEmployeeComponent,
    ListEmployeeComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    SweetAlert2Module.forRoot(),
    AppRoutingModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
