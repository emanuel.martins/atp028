import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  private localStorage: Storage;

  constructor() {
    this.localStorage = window.localStorage;
  }

  get(key: string): string | null {
    return this.localStorage.getItem(key) ?? 'Invalid key';
  }

  set(key: string, value: string): void {
    this.localStorage.setItem(key, value);
  }
}
