export interface IFuncionario {
  nome: string;
  cargo: string | number;
  salario: number | null;
}
